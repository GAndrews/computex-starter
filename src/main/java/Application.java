import java.util.Scanner;

public class Application {
    static boolean appIsActive = true;
    static Frontend frontend;

    public static void main(String[] args) {
        //Typically the main method is used to create your actual "app". This is normally a class which contains your applications logic
        frontend = new Frontend();
        while( appIsActive) {
            frontend.start();
        }
    }
}
