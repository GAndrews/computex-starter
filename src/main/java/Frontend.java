import java.util.Scanner;

public class Frontend {

    Scanner scanner = new Scanner(System.in);   //Declaring this here means we only have one for the entire instance

    static final String banner                                                             //The final keyword means this variable cannot
            = ">>>>--------------------------------------<<<<" + System.lineSeparator() +  // be written to. It is Immutable
              ">>>>         WELCOME TO COMPUTEX          <<<<" + System.lineSeparator() +
              ">>>>--------------------------------------<<<<" + System.lineSeparator();

    public void start() {
        printWelcomeMessage();
        String input = ""; //This will be used to record the user inputs
        input = scanner.next(); //Read the next string the user inputs, this pauses the program

        int number = scanner.nextInt();//Read in the next number provided by the user (throws an error when text is read in)
    }

    public String printWelcomeMessage() {
        System.out.println( banner );
        return banner;
    }




}
