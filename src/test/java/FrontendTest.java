import org.junit.Before;


import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class FrontendTest {

    Frontend frontend;

    @Before
    public void setup() {
        frontend = new Frontend();
    }

    @Test
    public void check_the_banner_contains_Computex() {
        assertTrue("The welcome banner doesn't contain the words 'Computex'", frontend.printWelcomeMessage().contains("COMPUTEX"));
    }
}
