package cucumberJava;

import cucumber.api.java.en.*;

public class StepDefinitions {

    @Given("I have a blank order")
    public void blankOrder() {

    }

    @When("I add a (.*)")
    public void addAnItem(String args0) {

    }

    @Then("I should have a (.*) on my list")
    public void checkItemIsInTheOrder(String argos0) {

    }
}
